﻿function toggleDropdown() {
    var dropdownMenu = document.getElementById("dropdownMenu");
    if (dropdownMenu.style.display === "none" || dropdownMenu.style.display === "") {
        dropdownMenu.style.display = "block";
    } else {
        dropdownMenu.style.display = "none";
    }
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {
    var dropdownMenu = document.getElementById("dropdownMenu");
    if (!event.target.matches('.dropdown-toggle')) {
        if (dropdownMenu.style.display === "block") {
            dropdownMenu.style.display = "none";
        }
    }
}